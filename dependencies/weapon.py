import pygame
from dependencies.settings import player_weapon_size, weapon_speed, frameHeight, weapon_size


class Bullet(pygame.sprite.Sprite):
    def __init__(self, pos, side, y_speed, x_speed = 0):
        super().__init__()
        self.x = pos[0]
        self.y = pos[1]
        if side == "enemy":
            self.image = pygame.image.load("assets/images/enemy-bullet.png")
            self.image = pygame.transform.scale(self.image, (weapon_size, weapon_size))
            self.rect = self.image.get_rect(topleft = pos)
            self.mask = pygame.mask.from_surface(self.image)
            self.y_move_speed = weapon_speed
            self.x_move_speed = 0
        if side == "player":
            self.damage_value = 1
            self.image = pygame.image.load("assets/images/player-bullet.png")
            self.image = pygame.transform.scale(self.image, (player_weapon_size, player_weapon_size*1.2))
            self.rect = self.image.get_rect(topleft = pos)
            self.mask = pygame.mask.from_surface(self.image)
            self.y_move_speed = (- (weapon_speed+x_speed))
            self.x_move_speed = y_speed

    def __str__(self):
        return f"{self.rect.topleft}"

    def move_bullet(self):
        self.rect.y += self.y_move_speed
        self.rect.x += self.x_move_speed


    def update(self):
        self.move_bullet()
        self.rect = self.image.get_rect(topleft = (self.rect.x, self.rect.y))
        if self.rect.bottom <= 10 or self.rect.top >= frameHeight:
            self.kill()


class Missile(pygame.sprite.Sprite):
    def __init__(self, pos):
        super().__init__()
        self.x = pos[0]
        self.y = pos[1]
        self.damage_value = 3
        self.image = pygame.image.load("assets/images/rocket.png")
        self.image = pygame.transform.scale(self.image, (weapon_size, weapon_size*2))
        self.rect = self.image.get_rect(topleft = pos)
        self.mask = pygame.mask.from_surface(self.image)
        self.move_speed = (-weapon_speed)

    def hit(self):
        self.image = pygame.image.load("assets/images/rocket.png")
        self.image = pygame.image.transform(self.image, (weapon_size*4, weapon_size*4))
        self.mask = pygame.mask.from_surface(self.image)

    def move_missile(self):
        self.rect.y += self.move_speed


    def update(self):
        self.move_missile()
        self.rect = self.image.get_rect(topleft = (self.rect.x, self.rect.y))
        if self.rect.bottom <= 0 or self.rect.top >= frameHeight:
            self.kill()
