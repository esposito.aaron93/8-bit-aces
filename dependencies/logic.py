from random import randint
import pygame
from dependencies.settings import plane_size, frameWidth, frameHeight

def enemy_move():
    move = randint(1,100)
    if move <= 30:
        return 'l'
    elif move > 30 and move <= 60:
        return 'r'
    else:
        return 'd'


def plane_select():
    images = [
        "assets/images/GER_bf109.png",
        "assets/images/GER_bf110.png",
        "assets/images/GER_Ju87.png",
    ]
    planes = []
    pos = [(frameWidth//2-plane_size//2) - 15, frameHeight//2-plane_size]
    for image in images:
        img = pygame.image.load(image)
        img = pygame.transform.scale(img, (plane_size, plane_size))
        img = pygame.transform.rotate(img, 45)
        rect = img.get_rect(topleft = pos)
        pos[1] += 100
        planes.append([img, rect, image])
    return planes

def button_click_check(plane, mouse_coords):
    if plane[0] <= mouse_coords[0] <= plane[0]+plane_size and plane[1] <= mouse_coords[1] <= plane[1]+plane_size:
        return True
