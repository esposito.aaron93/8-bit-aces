#file used to store constant values for game logic

frameHeight = 800
frameWidth = 400
space = 30
font_size = 20
event_font_size = 60
nav_thickness = 50
plane_size = 90
player_speed = 6
enemy_speed = 2
weapon_speed = 10
weapon_size = 10
player_weapon_size = 10
power_size = 40
