import pygame

pygame.mixer.init()
pygame.mixer.music.set_volume(0.25)
shoot = pygame.mixer.Sound("assets/music/bullet.ogg")
boom = pygame.mixer.Sound("assets/music/explosion06.wav")
enemy_shoot = pygame.mixer.Sound("assets/music/bullet2.ogg")
player_hit = pygame.mixer.Sound("assets/music/player_hit.ogg")
boom.set_volume(2.0)
shoot.set_volume(0.2)
enemy_shoot.set_volume(0.5)
player_hit.set_volume(0.5)

def sound_shoot():
    pygame.mixer.Sound.play(shoot)

def sound_enemy_shoot():
    pygame.mixer.Sound.play(enemy_shoot)

def sound_player_hit():
    pygame.mixer.Sound.play(player_hit)

def sound_explode():
    pygame.mixer.Sound.play(boom)


def music(char):
    pygame.mixer.music.set_volume(0.25)
    pygame.mixer.music.stop()
    pygame.mixer.music.unload()
    pygame.mixer.music.load("assets/music/background-music.ogg")
    pygame.mixer.music.play(-1)

    if char == 'S':
        pygame.mixer.music.set_volume(0.25)
        pygame.mixer.music.stop()
        pygame.mixer.music.unload()
        pygame.mixer.music.load("assets/music/theme.mp3")
        pygame.mixer.music.play(-1)

    elif char == 'L':
        pygame.mixer.music.set_volume(0.50)
        pygame.mixer.music.stop()
        pygame.mixer.music.unload()
        pygame.mixer.music.load("assets/music/10_plus.ogg")
        pygame.mixer.music.play(-1)

    if char == 'D':
        pygame.mixer.music.set_volume(0.25)
        pygame.mixer.music.stop()
        pygame.mixer.music.unload()
        pygame.mixer.music.load("assets/music/game_over.ogg")
        pygame.mixer.music.play(-1)
