import pygame
import time
from dependencies.player import Player
from dependencies.bandits import Bandit
from dependencies.power_ups import Item
from dependencies.settings import frameHeight, frameWidth, plane_size, nav_thickness
from dependencies.display import Display
from dependencies.logic import enemy_move
from dependencies.sounds import music, sound_explode, sound_enemy_shoot, sound_player_hit
from random import randint


class World:
    def __init__(self, screen, player_sprite):
        self.screen = screen
        self.player = pygame.sprite.GroupSingle()
        self.player_sprite = player_sprite
        self.bandits = pygame.sprite.Group()
        self.powerups = pygame.sprite.Group()
        self.game_over = False
        self.player_score = 0
        self.game_level = 1
        self.display = Display(self.screen, self.player_sprite)
        self.spawn = 0
        self.recent_collision = False
        self.flash_count = 0
        self.music_state = "S"
        self.previous_music = "S"
        music(self.music_state)
        self.generate_player()
        self.generate_bandits()

    def generate_player(self):
        player_x, player_y = frameWidth //2, frameHeight - plane_size
        center_size = plane_size // 2
        player_pos = (player_x - center_size, player_y-(frameHeight*.15))
        self.player.add(Player(player_pos, self.player_sprite))

    def generate_bandits(self):
        for _ in range(0, (1 if self.game_level == 1 else self.game_level//4)):
            self.bandits.add(Bandit((randint(25, 375), randint(-200, -50)), _, self.game_level//4))

    def add_additionals(self):
        nav = pygame.Rect(0, frameHeight, frameWidth, nav_thickness)
        pygame.draw.rect(self.screen, pygame.Color("blue"), nav)
        self.display.show_life(self.player.sprite.life)
        self.display.show_score(self.player_score)
        self.display.show_level(self.game_level)
        self.display.show_ammo(self.player.sprite.missile_count)

    def player_move(self):
        self.player.sprite.image.set_alpha(255)
        keys = pygame.key.get_pressed()
        if keys[pygame.K_a] and not self.game_over or keys[pygame.K_LEFT] and not self.game_over:
            if self.player.sprite.rect.left > 0:
                self.player.sprite.move_left()
        if keys[pygame.K_d] and not self.game_over or keys[pygame.K_RIGHT] and not self.game_over:
            if self.player.sprite.rect.right < frameWidth:
                self.player.sprite.move_right()
        if keys[pygame.K_w] and not self.game_over or keys[pygame.K_UP] and not self.game_over:
            if self.player.sprite.rect.top > 0:
                self.player.sprite.move_up()
        if keys[pygame.K_s] and not self.game_over or keys[pygame.K_DOWN] and not self.game_over:
            if self.player.sprite.rect.bottom < frameHeight:
                self.player.sprite.move_down()


    def detect_collision(self):
        player_powerup_collision = pygame.sprite.groupcollide(self.powerups, self.player, True, False)
        for power in player_powerup_collision:
            self.player.sprite.power = power.grabbed()
        player_attack_collision = pygame.sprite.groupcollide(self.bandits, self.player.sprite.player_bullets, False, True)
        if player_attack_collision:
            sound_explode()
            print(player_attack_collision)
            for key in player_attack_collision:
                key.hit(player_attack_collision[key][0].damage_value)
                if key.life <= 0 and key.has_upgrade:
                    location = [key.rect.x, key.rect.y]
                    self.powerups.add((Item(location, key.upgrade_value)))
                    key.kill()
                if key.life <= 0:
                    key.kill()
            self.player_score += 10*self.game_level

        for bandit in self.bandits.sprites():
            bandit_attack_collision = pygame.sprite.groupcollide(bandit.bullets, self.player, True, False)
            if bandit_attack_collision:
                self.player.sprite.life -= 1
                self.recent_collision = True
                sound_player_hit()
                break

        bandit_to_player_collision = pygame.sprite.groupcollide(self.bandits, self.player, True, False)
        if bandit_to_player_collision:
            sound_explode()
            self.player.sprite.life -= 1
            self.recent_collision = True
            sound_player_hit()

        for bandit in self.bandits.sprites():
            if bandit.rect.top >= frameHeight:
                self.player.sprite.life -= 2
                bandit.kill()
                break

    def collision_flash(self):
        if self.recent_collision == True  and self.flash_count < 7:
            if self.flash_count % 2 == 0:
                self.player.sprite.image.set_alpha(140)
            else:
                self.player.sprite.image.set_alpha(255)
            self.flash_count += 1
        else:
            self.recent_collision = False
            self.flash_count = 0

    def bandit_move(self):
        for bandit in self.bandits.sprites():
            bandit.change_dir_delay += 1
            if bandit.change_dir_delay % bandit.delay == 0:
                bandit.dir = enemy_move()
            if bandit.dir == 'l':
                if bandit.rect.left > 0:
                    bandit.move_left()
                else:
                    bandit.dir = "r"
            if bandit.dir == 'r':
                if bandit.rect.right <= frameWidth :
                    bandit.move_right()
                else:
                    bandit.dir = "l"
            if bandit.dir == 'd':
                bandit.move_down()

    def bandit_shoot(self):
        for bandit in self.bandits.sprites():
            if bandit.rect.bottom > 0:
                bandit.shot_delay += 1
                if bandit.shot_delay % 50 == 0 or bandit.shot_delay % 90 == 0:
                    bandit.shoot()
                    sound_enemy_shoot()

    def powerup_decay(self):
        for powerup in self.powerups:
            powerup.ttl()

    def _check_game_state(self):
        if self.player.sprite.life <= 0:
            self.game_over = True
            self.display.game_over_message()
            self.music_state = 'D'
            self.is_music_gameover = "Y"
            with open("assets/scores/score.txt", "r+",  encoding="utf-8") as f:
                read = f.readlines()
                if self.player_score > int(read[-1]):
                    f.write( f'{str(self.player_score)} \n')
        if len(self.bandits) == 0 and self.player.sprite.life > 0:
            self.game_level += 1
            if self.game_level >= 10:
                self.music_state = 'L'
            if self.player.sprite.life < 3:
                self.player.sprite.life += 1
            self.generate_bandits()
            for bandit in self.bandits.sprites():
                if bandit.move_speed <= 10:
                    bandit.move_speed += .1

    def set_music(self):
        if self.previous_music != self.music_state:
            music(self.music_state)
            self.previous_music = self.music_state

    def update(self):
        self.set_music()
        self.detect_collision()
        self.collision_flash()
        self.bandit_move()
        self.bandit_shoot()
        self.powerup_decay()
        self.player.sprite.player_bullets.update()
        self.player.sprite.player_bullets.draw(self.screen)
        self.powerups.draw(self.screen)
        [bandit.bullets.update() for bandit in self.bandits.sprites()]
        [bandit.bullets.draw(self.screen) for bandit in self.bandits.sprites()]
        self.player.update()
        self.player.draw(self.screen)
        self.bandits.draw(self.screen)
        self.add_additionals()
        self._check_game_state()
