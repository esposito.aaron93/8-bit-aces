import pygame
from dependencies.settings import weapon_size, enemy_speed, plane_size
from dependencies.weapon import Bullet
from random import randint

enemy_images = {
    1:"assets/images/GER_bf109.png",
    2:"assets/images/GER_bf110.png",
    3:"assets/images/GER_FW190.png",
    4:"assets/images/GER_HE111.png",
    5:"assets/images/GER_Ju87.png",
    }


class Bandit(pygame.sprite.Sprite):
    def __init__(self, pos, id, level):
        super().__init__()
        self.x = pos[0]
        self.y = pos[1]
        self.life = int(1.2*level)
        self.image = pygame.image.load(enemy_images[randint(1, 5)])
        self.image = pygame.transform.scale(self.image, (plane_size, plane_size))
        self.image = pygame.transform.rotate(self.image, 180)
        self.move_speed = enemy_speed
        self.rect = self.image.get_rect(topleft = pos)
        self.mask = pygame.mask.from_surface(self.image)
        self.bullets = pygame.sprite.Group()
        self.shot_delay = 0
        self.change_dir_delay = randint(10, 50)
        self.spawn_delay = randint(0,5)
        self.id = id
        self.delay = randint(10, 50)
        self.dir = "d"
        self.has_upgrade = True if (randint(0,10)) >= 8 else False
        if self.has_upgrade:
            self.upgrade_value = randint(1, 3)

    def __str__(self):
        return(f"{self.id} {self.dir}")

    def move_left(self):
        self.rect.x -= self.move_speed
        self.rect.y += self.move_speed*.75
    def move_right(self):
        self.rect.x += self.move_speed
        self.rect.y += self.move_speed *.75
    def move_down(self):
        self.rect.y += self.move_speed

    def shoot(self):
        specific_pos = ((self.rect.centerx - weapon_size // 2), self.rect.centery)
        self.bullets.add(Bullet(specific_pos, "enemy", 0))
        self.rect = self.image.get_rect(topleft=(self.rect.x, self.rect.y))

    def hit(self, damage):
        self.life -= damage
