import pygame

from dependencies.settings import frameHeight, frameWidth, space, font_size, event_font_size

pygame.font.init()

class Display:
    def __init__(self, screen, sprite):
        self.screen = screen
        self.life_image = sprite
        self.score_font = pygame.font.SysFont("monospace", font_size)
        self.level_font = pygame.font.SysFont("impact", font_size)
        self.event_font = pygame.font.SysFont("impact", event_font_size)
        self.text_color = pygame.Color("black")
        self.event_color = pygame.Color("red")

    def show_life(self, life):
        life_size = 30
        life_image = pygame.image.load(self.life_image)
        life_image = pygame.transform.scale(life_image, (life_size, life_size))
        life_x = space // 2
        if life != 0:
            for life in range(life):
                self.screen.blit(life_image, (life_x, 750))
                life_x += life_size

    def show_ammo(self, ammo):
        ammo_image = pygame.image.load("assets/images/rocket.png")
        ammo_image = pygame.transform.scale(ammo_image, (10, 40))
        ammo_x = space // 2
        ammo_y = 700
        if ammo != 0:
            for ammo in range(ammo):
                self.screen.blit(ammo_image, (ammo_x, ammo_y))
                ammo_x += 10

    def show_score(self, score):
        score_x = frameWidth // 3
        score = self.score_font.render(f'score: {score}', True, self.text_color)
        self.screen.blit(score, (score_x, 750))

    def show_level(self, level):
        level_x = frameWidth // 3
        level = self.level_font.render(f'Level {level}', True, self.text_color)
        self.screen.blit(level, (level_x * 2+25, 750))

    def game_over_message(self):
        message = self.event_font.render('GAME OVER!', True, self.event_color)
        self.screen.blit(message, (70, (frameHeight // 2) - (event_font_size // 2)))

    def show_high_score(self):
        score_x = frameWidth // 3
        with open("assets/scores/score.txt", "r+",  encoding="utf-8") as f:
            read = f.readlines()
            h_score = read[-1]
        self.screen.blit(h_score, (score_x, 20))
