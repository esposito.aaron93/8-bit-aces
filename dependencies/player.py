import pygame
from dependencies.settings import player_speed, player_weapon_size, plane_size
from dependencies.weapon import Bullet, Missile
from time import sleep


class Player(pygame.sprite.Sprite):
    def __init__(self, pos, player_sprite):
        super().__init__()
        self.x = pos[0]
        self.y = pos[1]
        self.image = pygame.image.load(player_sprite)
        self.image = pygame.transform.scale(self.image, (plane_size, plane_size))
        self.rect = self.image.get_rect(topleft = pos)
        self.mask = pygame.mask.from_surface(self.image)
        self.plane_speed = player_speed
        self.life = 3
        self.player_bullets = pygame.sprite.Group()
        self.missile_count = 3
        self.power = 0

    def move_left(self):
        self.rect.x -= self.plane_speed
    def move_up(self):
        self.rect.y -= self.plane_speed
    def move_right(self):
        self.rect.x += self.plane_speed
    def move_down(self):
        self.rect.y += self.plane_speed


    def shoot(self):
        specific_pos = (self.rect.centerx - (player_weapon_size // 2), self.rect.y)
        if self.power == 0:
            self.player_bullets.add(Bullet(specific_pos, "player", 0))
        elif self.power == 1:
            self.player_bullets.add(Bullet(specific_pos, "player", 0))
            self.player_bullets.add(Bullet(specific_pos, "player", 1))
            self.player_bullets.add(Bullet(specific_pos, "player", -1))
            print('shoot')
        elif self.power == 2:
            self.player_bullets.add(Bullet((specific_pos[0] + 20, specific_pos[1]), "player", 0))
            self.player_bullets.add(Bullet((specific_pos[0] - 20, specific_pos[1]), "player", 0))
        elif self.power == 3:
            for i in range(5):
                self.player_bullets.add(Bullet(specific_pos, "player", 0,  i))


    def launch(self):
        if self.missile_count > 0:
            self.missile_count -= 1
            specific_pos = (self.rect.centerx + 20 - (player_weapon_size // 2), self.rect.y)
            self.player_bullets.add(Missile(specific_pos))

    def update(self):
        self.rect = self.image.get_rect(topleft=(self.rect.x, self.rect.y))
