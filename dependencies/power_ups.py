import pygame
from dependencies.settings import power_size
from random import random


class Item(pygame.sprite.Sprite):
    def __init__(self, pos, power_up):
        super().__init__()
        self.x = pos[0]
        self.y = pos[1]
        self.power_up = power_up
        self.image = pygame.image.load(power_ups[power_up]["image"])
        self.image = pygame.transform.scale(self.image, (power_size, power_size))
        self.rect = self.image.get_rect(topleft = pos)
        self.mask = pygame.mask.from_surface(self.image)
        self.uptime = 300

    def __str__(self):
        return(f"{self.x} {self.y}")

    def grabbed(self):
        return self.power_up

    def ttl(self):
        self.uptime -= 1
        if self.uptime <= 0:
            self.kill()


power_ups = {
    1:{
        "image": "assets\images\split.png"
    },
    2:{
        "image": "assets\images\double.png"
    },
    3:{
        "image": "assets/images/rapid.png"
    }

}
