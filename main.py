import pygame
from dependencies.settings import frameHeight, frameWidth, font_size
from dependencies.world import World
from dependencies.sounds import sound_shoot
from dependencies.buttons import button
from dependencies.logic import plane_select, button_click_check
from dependencies.sounds import music


pygame.init()

screen = pygame.display.set_mode((frameWidth, frameHeight))
pygame.display.set_caption("8-bit Aces")
bg = pygame.image.load("assets\images\plane.png")
pygame.display.set_icon(bg)
tiles = (frameHeight / bg.get_height()) + 1
scroll = 0
button_font = pygame.font.SysFont("impact", font_size)
text = button_font.render('Start', True, "black")

class Main:
    def __init__(self, screen, bg):
        self.screen = screen
        self.fps = pygame.time.Clock()
        self.bg = pygame.image.load(bg)
        self.tiles = (frameHeight / self.bg.get_height()) + 1
    def main(self, player_sprite):
        world = World(self.screen, player_sprite)
        scroll = 0
        while 1:
            if world.game_level > 10:
                self.bg = pygame.image.load("assets/images/gravel-biome.png")
            else:
                self.bg = pygame.image.load("assets/images/grass-biome2.png")
            #set framerate
            self.fps.tick(30)
            #break game loop on "x" window button click
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    quit()
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        world.player.sprite.shoot()
                        sound_shoot()
                    if event.key == pygame.K_r:
                        self.flow()
                    if event.key == pygame.K_f:
                        world.player.sprite.launch()
            #scroll background image down screen frame
            i = 0
            while(abs(i) < tiles):
                screen.blit(self.bg, (0,(self.bg.get_height()*i-scroll)))
                i -= 1

            #control scroll rate
            scroll -= 1

            #reset scroll position after the image has scrolled the length of the image
            if abs(scroll) > self.bg.get_height():
                scroll = 0

            world.player_move()
            world.update()
            if pygame.display.update():
                self.flow()

    def menu(self):
        music("_")
        img = pygame.image.load("assets/images/grass-biome2.png")
        title = pygame.image.load("assets/images/title.png")
        subtitle = pygame.image.load("assets/images/subtitle.png")
        planes = plane_select()
        with open("assets/scores/score.txt", "r+",  encoding="utf-8") as f:
                read = f.readlines()
                h_score = read[-1][:-1]
        score_font = pygame.font.SysFont("monospace", font_size)
        h_text = score_font.render(f'High Score: {h_score}', True, pygame.Color("black"))
        while 1:
            self.fps.tick(30)
            screen.blit(img, (0,0))
            screen.blit(title,(0, 50))
            screen.blit(subtitle,(0, 250))
            screen.blit(h_text, ((frameWidth // 3) - 15, 20))
            for plane in planes:
                screen.blit(plane[0], plane[1])
            mouse = pygame.mouse.get_pos()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    quit()
                if event.type == pygame.MOUSEBUTTONDOWN:
                    for plane in planes:
                        if button_click_check(plane[1], mouse):
                            return plane[2]
            pygame.display.update()

    def flow(self):
        player_sprite = self.menu()
        self.main(player_sprite)


if __name__ == "__main__":
    play = Main(screen, "assets/images/grass-biome2.png")
    play.flow()
